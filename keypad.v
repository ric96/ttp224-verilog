module keypad (
input clk,
input KEY_SDA,
output KEY_SCL,
output [7:0] key_out,
);

reg [7:0] key;
reg [3:0] counter;
reg scl = 0;

always @(posedge clk) begin
	case(counter)
		1: begin
			if(KEY_SDA == 1) key = "0";
			//else key = 0;
		end
		2: begin
			if(KEY_SDA == 1) key = "1";
			//else key = 0;
		end
		3: begin
			if(KEY_SDA == 1) key = "2";
			//else key = 0;
		end
		4: begin
			if(KEY_SDA == 1) key = "3";
			//else key = 0;
		end
		5: begin
			if(KEY_SDA == 1) key = "4";
			//else key = 0;
		end
		6: begin
			if(KEY_SDA == 1) key = "5";
			//else key = 0;
		end
		7: begin
			if(KEY_SDA == 1) key = "6";
			//else key = 0;
		end
		8: begin
			if(KEY_SDA == 1) key = "7";
			//else key = 0;
		end
		9: begin
			if(KEY_SDA == 1) key = "8";
			//else key = 0;
		end
		10: begin
			if(KEY_SDA == 1) key = "9";
			//else key = 0;
		end
		11: begin
			if(KEY_SDA == 1) key = "A";
			//else key = 0;
		end
		12: begin
			if(KEY_SDA == 1) key = "B";
			//else key = 0;
		end
		13: begin
			if(KEY_SDA == 1) key = "C";
			//else key = 0;
		end
		14: begin
			if(KEY_SDA == 1) key = "D";
			//else key = 0;
		end
		15: begin
			if(KEY_SDA == 1) key = "E";
			//else key = 0;
		end
		16: begin
			if(KEY_SDA == 1) key = "F";
			//else key = 0;
		end
		//default: key = 0;
	endcase
	//if(KEY_SDA == 0) key = 0;
	counter <= counter + 1;
end

assign KEY_SCL = clk;
assign key_out = key;

endmodule